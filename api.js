const {Router, query} = require("express");
const visionareUser = require("./models/visionare-users.js")
const artMeta = require("./models/art.js")
const commentMeta = require("./models/comments.js")
const apiRouter = Router();
const path = require("path");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const saltRounds = 10;


apiRouter.get("/user/findOne/", async function (req, res){
	try {
		const user = req.query.username;
		if (!user) {
			return res.status(404).send("User not specified.")
		}
		await visionareUser.findOne(
			{username: req.query.username},
			"username bio pfpSrc").then((foundUser) => {
				const jsonRes = {"UID": foundUser._id, "username":foundUser.username, "bio":foundUser.bio, "pfpSrc": foundUser.pfpSrc}
				res.status(200).send(jsonRes);
			}
		)}
		catch(err) {
			console.error(err);
		}
	}
);

apiRouter.get("/user/findOneByID/", async function (req, res){
	try {
		const userID = req.query.UID;
		if (!userID) {
			return res.status(404).send("User not specified.")
		}
		await visionareUser.findOne(
			{_id: userID},
			"username bio pfpSrc").then((foundUser) => {
				const jsonRes = {"UID": foundUser._id, "username":foundUser.username, "bio":foundUser.bio, "pfpSrc": foundUser.pfpSrc}
				res.status(200).send(jsonRes);
			}
		)}
		catch(err) {
			console.error(err);
		}
	}
);

apiRouter.get("/user/findFieldIn/", async function (req, res) {
	// req = {user: String, field: String}
	try {
		const user = req.query.username;
		const field = req.query.field;
		if (!user) {
			return res.status(404).send("User not specified.")
		}
		await visionareUser.findOne(
			{username: req.query.username, _id: 0}, `${field}`).then((found) => {
				res.status(200).send(found);
			})
		}
		catch(err) {
			console.error(err);
		}
	}
);


apiRouter.get("/user/findAll/", async function (req, res){
	try {
		await visionareUser.find({}).then((found) => {
			res.status(200).send(found);
		});
	} catch(err) {
		console.error(err);
	}
});

apiRouter.post("/user/sign_up", function(req, res){
	try {
		bcrypt.hash(req.body.pass, saltRounds, function(err, hash){
			if (err) return console.error(err);
			const newUser = new visionareUser({
			fName: req.body.fName,
			lName: req.body.lName,
			username: req.body.username,
			userAge: req.body.age,
			password: hash,
			pfpSrc: __dirname + "/public/asset/userPFP/default.png",
			contributor: false,
			bio: "",
			likedPosts: [],
			favoritePosts: [],
				})
			const token = jwt.sign(
				{ 	name: req.body.fName,
					user: req.body.username,
					contributor: false, },
				process.env.TOKEN_KEY,
				{
					expiresIn: "2h",
				}
			);
			newUser.token = token;
			newUser.save(function (err, newUser) {
				if (err) return console.error(err);
				console.log(newUser.username + " saved to database.");
			})
			res.status(201).send({body: token});
		});
		} catch(err) {
		console.error(err);
		res.sendStatus(500);
	}});

apiRouter.post("/user/log_in", async function(req, res){
	try {
		await visionareUser.findOne( 
				{ username: req.body.username },
				"username password fName lName").then((foundUser) => {
					bcrypt.compare(req.body.password, foundUser.password).then((result) => {
						if (result) {
							const token = jwt.sign(
								{
									name: foundUser.fName,
									user: foundUser.username,
									contributor: foundUser.contributor,
									},
								process.env.TOKEN_KEY,
								{
									expiresIn: "2h",
								}
							);
							return res.status(200).send({ body:token, status:200});
						} else {
							console.log("Incorrect username / password combo.")
							res.sendStatus(500);
						}
					})
				})
	} catch(err) {
		console.error(err);
		res.sendStatus(400);
	}
	});

apiRouter.patch("/user/saveBio/", async function (req, res){
	try {
		await visionareUser.updateOne({username: req.body.username}, {bio: req.body.bio})
		res.status(200).send({body: "bio updated"});
	} catch(err) {
		console.error(err);
	}
});

apiRouter.patch("/user/updatePfpSrc/", async function (req, res) {
	try {
		console.log(req.body);
		await visionareUser.updateOne({username: req.body.username}, {pfpSrc: req.body.pfpSrc});
		res.sendStatus(200);
	}
	catch(err) {
		console.error(err);
		res.sendStatus(500);
	}
});

apiRouter.patch("/user/modifyFollowerCount", async function(req, res) {
	//req = {body: {willAdd:Bool, UID:String} }
	try{
		const amt = req.body.willAdd ? 1 : -1;
		await visionareUser.findOneAndUpdate({_id: req.body.UID}, {$inc: {followers: amt}}).then(() => {
			res.status(200).send({body: `Added ${amt} to followers in ${req.body.UID}`});
		})
	} catch(err){
		console.error(err);
		res.sendStatus(500);
	}
})

apiRouter.patch("/user/pushToArrField/", async function (req, res){
	// req = {body: {username: String, field: String, SID: String}}
	try {
		const activeUser = req.body.username;
		const field = req.body.field;
		await visionareUser.findOneAndUpdate({username: activeUser},
			 {$push: {[field]: req.body.SID}}).then(() => {
				res.status(200).send({body: `Added ${req.body.SID} to ${activeUser}.${field}`})
			 })
	}catch (err){
		console.error(err);
	}
});

apiRouter.patch("/user/pullFromArrField/", async function (req, res){
	//req = {body: {username: String, field: String, SID: String}}
	try {
		const activeUser = req.body.username;
		const field = req.body.field;
		await visionareUser.findOneAndUpdate({username: activeUser},
			 {$pull: {[field]: req.body.SID}}).then(() => {
				res.status(200).send({body: `Removed ${req.body.SID} from ${activeUser}.${field}`})
			 })
	}catch (err){
		console.error(err);
	}
});

apiRouter.patch("/user/findIDInArrField", async function (req, res) {
	// req = {body: {username:String, SID:String, field:String}}
	try {
		const currentUser = req.body.username;
		const searchID = req.body.SID;
		const field = req.body.field;
		let isFound;
		await visionareUser.find({"username": currentUser}, {[field]: 1, _id: 0}).then((result) => {
			let fieldValues = result[0][field];
			isFound = (fieldValues.includes(searchID)) ? true : false;
			res.status(200).send({"result": isFound});
		});
	} catch(err){
		console.error(err);
		res.sendStatus(500);
	}
})
	

apiRouter.get("/art/findOne", async function(req, res){
	try {
		const title = req.query.title;
		await artMeta.findOne({title: title}, "title author src description")
		.then((foundArt) => {
			const jsonRes = {"AID": foundArt._id, "title":foundArt.title, "author":foundArt.author, "src": foundArt.src, "description": foundArt.description}
			res.status(200).send(jsonRes);
		})
	} catch(err) {
		console.error(err);
		res.sendStatus(600);
	}
});

apiRouter.get("/art/findAll/", async function (req, res){
	try {
		await artMeta.find({}).then((found) => {
			res.status(200).send(found);
		});
	} catch(err) {
		console.error(err);
	}
});

apiRouter.get("/art/findbyArtist/", async function (req, res){
	try {
		const queryArtist = req.query.artist;
		await artMeta.find({author: queryArtist}).then((found) => {
			res.status(200).send(found);
		});
	} catch(err) {
		console.error(err);
	}
});

apiRouter.patch("/art/modifyArtScore", async function(req, res) {
	//req = {body: {field:String, willAdd:Bool, AID:String} }
	try{
		const field = req.body.field;
		const amt = req.body.willAdd ? 1 : -1;
		await artMeta.findOneAndUpdate({_id: req.body.AID}, {$inc: {[field]: amt}}).then(() => {
			res.status(200).send({body: `Added ${amt} to ${field} in ${req.body.AID}`});
		})
	} catch(err){
		console.error(err);
		res.sendStatus(500);
	}
})

apiRouter.post("/art/uploadMeta/", async function (req, res) {
	let tmpFile;
	let uploadPath;
	try{
		tmpFile = req.files.givenFile;
		internalPath = __dirname + "/public/pictures/" + tmpFile.name;
		uploadPath = '/visionare/pictures/' + tmpFile.name;
		tmpFile.mv(internalPath, function(err) {
			if (err) {
				console.error(err);
				res.sendStatus(500);
			}
		})

	}catch(err) {
		console.error(err)
		res.sendStatus(500);
	}
	try{
		const newPost = new artMeta({
		title: req.body.title,
		description: req.body.desc,
		src: uploadPath,
		tag1: "misc",
	})
	newPost.save(function (err, newUser) {
		if (err) return console.error(err);
		console.log(newPost.title + " saved to database.");
		res.redirect(`./utility.html?title=${req.body.title}`);
	})} catch(err) {
		console.error(err);
		res.sendStatus(500);
	}
});

apiRouter.patch("/art/saveAuthor/", async function (req, res){
	try {
		console.log(req.body);
		await artMeta.updateOne({title: req.body.title}, {author: req.body.author})
		.then(function() {
			res.status(200).send({body: "Bio Updated"});
		})
	} catch(err) {
		console.error(err);
	}
});



apiRouter.post("/uploadIMG/", async function (req, res) {
	let tmpFile;
	let uploadPath;
	try{
		tmpFile = req.files.givenFile;
		internalPath = __dirname + "/public/pictures/" + tmpFile.name;
		uploadPath = '/visionare/pictures/' + tmpFile.name;
		tmpFile.mv(internalPath, function(err) {
			if (err) {
				console.error(err);
				res.sendStatus(500);
			}
		})
		res.status(200).send({body: uploadPath})
	}catch(err) {
		console.error(err)
		res.sendStatus(500);
	}
})

apiRouter.post("/uploadPFP/", async function (req, res) {
	let tmpFile;
	let uploadPath;
	try{
		tmpFile = req.files.givenFile;
		internalPath = __dirname + "/public/asset/userPFP/" + tmpFile.name;
		uploadPath = '/visionare/pictures/' + tmpFile.name;
		tmpFile.mv(internalPath, function(err) {
			if (err) {
				console.error(err);
				res.sendStatus(500);
			}
		})
		res.redirect(`back`);
		}
	catch(err) {
		console.error(err);
		res.status(500).send("Failed to upload PFP: Internal Error");
	};
})

apiRouter.get("/decodeToken/", async function (req, res){
	try {
		const token = req.body.token || req.query.token || req.headers["x-acess-token"];
		if (!token) {
			return res.status(403).send("A token is required for authentication");
		}
		try {
			const decoded = jwt.verify(token, process.env.TOKEN_KEY);
			return res.status(200).send(decoded);
		} catch(err) {
			return res.status(401).send("Invalid Token");
		}
	} catch(err) {
		console.error(err);
	}
}
);

apiRouter.get("/comments/getAllComments", async function(req, res) {
	try {
		const reqAID = req.query.AID;
		if (!reqAID) {
			return res.status(403).send("An Art ID must be provided.")
		}
		commentMeta.find({AID: reqAID}).then((foundComments) => {
			res.status(200).send(foundComments);
		})
	} catch(err) {
		console.error(err);
		res.status(500).send("Internal error when gathering comments.")
	}
})

apiRouter.post("/comments/postComment", async function(req, res) {
	try {
		let today = new Date().toJSON().slice(0, 19);
		console.log(today);
		const newComment = new commentMeta({
			commentString: req.body.commentString,
			datePosted: today,
			UID: req.body.UID,
			AID: req.body.AID,
		});
		await newComment.save(function (err, newComment) {
			if (err) return console.error(err);
			console.log(newComment.commentString);
			console.log("Comment saved to database.");
		});
		res.status(201).send({body: "saved"});
	} catch(err) {
		console.error(err);
		res.status(500).send("Internal error when posting comment.")
	}
})




module.exports = apiRouter;
