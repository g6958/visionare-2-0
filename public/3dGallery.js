p5.disableFriendlyErrors = true;



let img = [];
let y = 0;
let camera;
let delta = 0.5;
let plane1 = 0;
let plane2 = 1;
let plane3 = 2;
let plane4 = 3;



function preload() {
  img[0] = loadImage(`./pictures/basketball.jpg`);
  img[1] = loadImage(`./pictures/90sgalaxy.png`);
  img[2] = loadImage(`./pictures/blueworld.png`);
  img[3] = loadImage(`./pictures/colorsoflife.png`);
  img[4] = loadImage(`./pictures/cone.jpg`);
  img[5] = loadImage(`./pictures/cube.jpg`);
  img[6] = loadImage(`./pictures/dawn.png`);
  img[7] = loadImage(`./pictures/fall.png`);
  img[8] = loadImage(`./pictures/fish.jpg`);
  img[9] = loadImage(`./pictures/globe.jpg`);
  img[10] = loadImage(`./pictures/happyhalloween.png`);
  img[11] = loadImage(`./pictures/irregularpolygon.jpg`);
  img[12] = loadImage(`./pictures/mountain.png`);
  img[13] = loadImage(`./pictures/peaceful.png`);
  img[14] = loadImage(`./pictures/rubikscube.jpg`);
  img[15] = loadImage(`./pictures/triangularpyramid.jpg`);
  img[16] = loadImage(`./pictures/VaporWave.png`);
  img[17] = loadImage(`./pictures/watermelon.jpg`);
  img[18] = loadImage(`./pictures/yummycake.png`);
}

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  pixelDensity(1);
  normalMaterial();
  frameRate(30);
  camera = createCamera(0, 0);
  camera.setPosition(0, 0, 0);
}

function draw() {
  background(0, 0, 0);
  //drag to move the world.
  //orbitControl();
  //translate(0, 0, 0);
  camera.pan(delta);
  console.log(frameCount);
  angleMode(DEGREES);
  //rectMode(CENTER);
  
  /*if (frameCount % 150 === 0  ) {
    plane1 = changeImage();
  } else if (frameCount % 201 === 0) {
    plane3 = changeImage();
  } else if (frameCount % 353 === 0) {
    plane2 = changeImage();
  } else if (frameCount % 552 === 0) {
    plane4 = changeImage();
  }*/
   
 if (frameCount % 330 === 0  ) {
    plane1 = changeImage();
    plane3 = changeImage();
  } else if (frameCount % 600 === 0) {
    plane2 = changeImage();
    plane4 = changeImage();
  }

  rotateImage(0, -3000, plane1);
  rotateImage(180, -3000, plane2);
  rotateImage(90, -3000, plane3);
  rotateImage(-90, -3000, plane4);
}

function rotateImage(a, b, c) {
  push();
  rotateY(a);
  translate(0, -200, b);
  texture(img[c]);
  plane(img[c].width, img[c].height);
  pop();
}

function changeImage(){
  randInt = Math.floor(Math.random() * 20);
  return randInt;
}