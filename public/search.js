const userCardTemplate = document.querySelector("[data-user-template]");
const userCardContainer = document.querySelector("[data-user-cards-container]");
const artCardTemplate = document.querySelector("[data-tag-template]");
const artCardContainer = document.querySelector("[data-tag-cards-container]");

const searchInput = document.querySelector("[data-search]");

const radioButtons = document.querySelectorAll('input[name="categorySearch"]');
const artistRadio = document.getElementById("Artist");
const artRadio = document.getElementById("Tag");


let users = [];
let arts = [];
let lastRadio = "";

//checks which radio was selected and switches the search
function checkRadio(radio) {
  clearResults();
  if (radio.value != lastRadio) {
    lastRadio = radio.value;
    radio.value == "Artist" ? artistSearch() : artSearch()
  }
  else {
    lastRadio = "";
    radio.checked = false;
  }
}

searchInput.addEventListener("click", function() {
  searchInput.placeholder == "";
})

function clearResults(){
      userCardContainer.replaceChildren(); 
      artCardContainer.replaceChildren();
}

async function artistSearch() {
  console.log("Artist Search");

  searchInput.addEventListener("input", (e) => {
    const value = e.target.value.toLowerCase();
    users.forEach((user) => {
      const isVisible =
        user.name.toLowerCase().includes(value) ||
        user.username.toLowerCase().includes(value);
      user.element.classList.toggle("hide", !isVisible);
      console.log(e);
    });
  });

  //results
  await fetch(`./api/user/findAll/`)
    .then((data) => data.json())
    .then((jsonData) => {
        users = jsonData.map((user) => {
        const card = userCardTemplate.content.cloneNode(true).children[0];
        card.href = `./userdisplay.html?username=${user.username}`;
        const header = card.querySelector("[data-header]");
        const body = card.querySelector("[data-body]");
        header.textContent = user.fName + " " + user.lName;
        body.textContent = user.username;
        userCardContainer.append(card);
        return { name: user.fName, username: user.username, element: card };
      });
    });
  }



function artSearch() {
  console.log("Art Search");
  searchInput.addEventListener("input", (e) => {
    const value = e.target.value.toLowerCase();
    arts.forEach((art) => {
      const isVisible =
        art.name.toLowerCase().includes(value) ||
        art.tagdesc.toLowerCase().includes(value);
      art.element.classList.toggle("hide", !isVisible);
      console.log(e);
    });
  });

  fetch(`./api/art/findAll/`)
    .then((res) => res.json())
    .then((data) => {
      arts = data.map((art) => {
        const card = artCardTemplate.content.cloneNode(true).children[0];
        card.href = `./artdisplay.html?title=${art.title}`;
        const header = card.querySelector("[data-tag-header]");
        const body = card.querySelector("[data-tag-body]");
        header.textContent = art.title;
        body.textContent = art.author;
        artCardContainer.append(card);
        return { name: art.title, tagdesc: art.author, element: card };
      });
    });
}




//Artist Search
/*searchInput.addEventListener("input", (e) => {
  const value = e.target.value.toLowerCase();
  users.forEach((user) => {
    const isVisible =
      user.name.toLowerCase().includes(value) ||
      user.username.toLowerCase().includes(value);
    user.element.classList.toggle("hide", !isVisible);
  });
});

//Tag Search
searchInput.addEventListener("input", (e) => {
  tags.forEach((tag) => {
    const isVisible =
      tag.name.toLowerCase().includes(value) ||
      tag.email.toLowerCase().includes(value);
    tag.element.classList.toggle("hide", !isVisible);
  });
});*/
