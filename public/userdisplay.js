const userPicture = document.getElementById("userPicture");
const bioText = document.getElementById("userBioText");
const imgContainer = document.getElementsByClassName("imgContainer");
const userName = document.getElementById("username");
const editButton = document.getElementById("editBio");
const cancelButton = document.getElementById("cancelEditBio");
const pfpButton = document.getElementById("changePFP");
const uploadForm = document.getElementById("uploadForm");
const avatarField = document.getElementById("avatar");
const bioEdit = document.getElementById("bioEditField");
const imgDisplayContainer = document.getElementById("lowerHalf");
const imgArr = [];
const fullUrl = window.location.href;
var newPFPFname = "";
var __pfpUploadPath = fullUrl.slice(0, fullUrl.lastIndexOf("/")) + "/asset/userPFP/";
var foundData;
var isLoggedIn = sessionStorage.token ? true : false;

gatherImgFromDB();

avatarField.addEventListener("change", previewPFP);


pfpButton.addEventListener("click", function() {uploadForm.classList.toggle("hidden")})

async function fetchTokenData() {
    if (!sessionStorage.getItem("token")){
        foundData = {user: getQS("username")};
    } 
    else {
        await fetch(`./api/decodeToken/?token=${sessionStorage.getItem("token")}`)
        .then((response) => response.json()
        .then((data) => {
            if (data.iat) {
                foundData = data;
                console.log("logged in as " + foundData.user);
                return foundData;
            }
            else {
                console.log("Session Expired!");
                terminateSession();
            }
        })
    )}
    return foundData;
}

function getQS(lookFor) {
    var query = window.location.search.substring(1);
    var params = query.split('&');
    for (var i=0; i<params.length; i++){
        var pos = params[i].indexOf("=");
        if (pos > 0 && lookFor == params[i].substring(0, pos)) {
            return params[i].substring(pos+1);
        }
    }
    return "";
}

async function getUserInfo() {
    var qs = getQS("username");
    var thisUser;
    await fetchTokenData();
    if (foundData) {
        qs ? thisUser=qs : thisUser=foundData.user;
        await fetch(`./api/user/findOne/?username=${thisUser}`)
            .then(function(userData) {
                return userData.json();
            }).then(function(userDataJSON) {
                setUserPageInfo(userDataJSON);
            })
        }
    }

async function setUserPageInfo(userInfo) {
    userInfo.bio ? bioText.innerText=userInfo.bio : bioText.innerText = "This user has no bio.";
    userInfo.pfpSrc ? userPicture.src=userInfo.pfpSrc : userPicture.src = `./asset/userPFP/default.png`;
    userName.innerText = userInfo.username;
    toggleUserActionButtons(userInfo.username);
}

function toggleUserActionButtons(thisPageUsername) {
    if (isLoggedIn && foundData.user == thisPageUsername) {
        editButton.classList.remove("hidden");
        editButton.onclick = editBio;
        pfpButton.classList.remove("hidden");
    }
    else {
        editButton.classList.add("hidden");
        pfpButton.classList.add("hidden");
        uploadForm.classList.add("hidden");
    }
}

function editBio() {
    bioEdit.classList.remove("hidden");
    //bioEdit.value = bioText.innerText;
    pfpButton.classList.add("hidden");
    cancelButton.classList.remove("hidden");
    editButton.firstChild.innerText = "Save";
    editButton.addEventListener('click', saveBio);
    cancelButton.addEventListener('click', cancelEdit);
    bioText.classList.add("hidden");
    document.getElementById("userInfo").appendChild(bioEdit);
}

function cancelEdit() {

}


function previewPFP() {
    var thisFile = avatarField.files[0];
    if (thisFile) {
        userPicture.src = URL.createObjectURL(thisFile);
    }
}

//document.getElementById("upload").addEventListener("click", uploadPFP)

uploadForm[1].onclick = async function(e) {
    e.preventDefault();
    newPFPFname = uploadForm[0].value.slice(12);
    await fetch(`./api/user/updatePfpSrc/`, {
        method: "PATCH",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
            username: sessionStorage.getItem("user"),
            pfpSrc: __pfpUploadPath + newPFPFname,
        })
    })
    .then(function() {
        uploadForm.submit();
    })
}

async function saveBio() {
    if (bioEdit) {
        console.log(bioEdit.value);
        var innerText = bioEdit.value;
        await fetch(`./api/user/saveBio/`, {
            method: "PATCH",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                username: foundData.user,
                bio: innerText,
            })
        }).then((response) => response.json())
        .then(function() {
            bioEdit.remove();
            location.reload();
        })
    }
}

async function gatherImgFromDB() {
    var qs = getQS("username");
    await fetch(`./api/art/findbyArtist?artist=${qs}`)
    .then((data) => data.json())
    .then((JSONdata) => {
        const tmpImgArr = [];
        for (let i = 0; i < JSONdata.length; i++) {
            let current = JSONdata[i];
            const newIMGElement = document.createElement("img");
            newIMGElement.classList.add("galleryImg");
            newIMGElement.src = current.src;
            newIMGElement.alt = current.title;
            newIMGElement.onclick = function () {
                window.location.href = `./artdisplay.html?title=${current.title}`;
            }
            if (current.tag3) {
                newIMGElement.setAttribute("tag3", current.tag3);
            } 
            if (current.tag2) {
                newIMGElement.setAttribute("tag2", current.tag2);
            }
            if (current.tag1) {
                newIMGElement.setAttribute("tag1", current.tag1);
            }
            imgArr.push(newIMGElement);
            imgDisplayContainer.appendChild(newIMGElement);
        }
    })
}

getUserInfo();