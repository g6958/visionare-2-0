function utilityPageOnload() {
    if (sessionStorage.getItem("token")){
        saveAuthorToDB();
    } else {
        window.location.replace("index.html");
    }
}

//function to strip query string from url for search purposes
function getQS(lookFor) {
    var query = window.location.search.substring(1);
    var params = query.split('&');
    for (var i=0; i<params.length; i++){
        var pos = params[i].indexOf("=");
        if (pos > 0 && lookFor == params[i].substring(0, pos)) {
            return params[i].substring(pos+1);
        }
    }
    return "";
}


//these functions are what handle the side nav bar opening / closing
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }
  
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
  }


//this code is what handles the collapsable text fields made famous on index

function collapsibleTextHandler() {
    var coll = document.getElementsByClassName("collapsible");
    var i;
    for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.maxHeight){
        content.style.maxHeight = null;
        } else {
        content.style.maxHeight = content.scrollHeight + "px";
        } 
    });
    }
}

//This is seperate because the upload.html/uploadMeta form submit doesn't prompt for author.
//Band-aid solution, but works. 1.0 drove me crazy
async function saveAuthorToDB() {
    const authorValue = sessionStorage.getItem("user");
    const titleValue = getQS("title");
    await fetch(`./api/art/saveAuthor`, {
        method: "PATCH",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({title: titleValue, author: authorValue})
    }).then(function() {
        sessionStorage.removeItem("user");
        sessionStorage.removeItem("title");
        window.location.replace(`./artdisplay.html?title=${titleValue}`)
    })
}
