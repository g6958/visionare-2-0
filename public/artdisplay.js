const titleContainer = document.getElementById("artname");
const authorContainer = document.getElementById("author");
const descContainer = document.getElementById("bioText");
const artDisplayContainer = document.getElementById("art");
const url = location.href;
const ABSPATH = url.substring(0, url.lastIndexOf("?"));
const ASSET = ABSPATH.substring(0, ABSPATH.lastIndexOf("/")) + "/asset/";
const socialSection = document.getElementById("social");
let loadedArtID = "";
let loadedArtistID = "";
const allComms = document.getElementById("comments");
const commentPrompt = document.getElementById("commentPrompt");
const userActionDict =  {
    like: "likedPosts",
    fav: "favoritePosts",
    follow: "followedUsers"
};

const artActionDict = {
    like: "likes",
    fav: "favorites",
    follow: "",
};

function getQS(lookFor) {
    var query = window.location.search.substring(1);
    var params = query.split('&');
    for (var i=0; i<params.length; i++){
        var pos = params[i].indexOf("=");
        if (pos > 0 && lookFor == params[i].substring(0, pos)) {
            return params[i].substring(pos+1);
        }
    }
    return "";
}

async function getArtInfo() {
    var qs = getQS("title");
    await fetch(`./api/art/findOne?title=${qs}`)
        .then(function(artData) {
            return artData.json();
        }).then(function(artDataJSON) {
            setArtPageInfo(artDataJSON);
        })
};

async function getArtistInfo() {
    var qs = authorContainer.innerText;
    console.log("author: " + qs);
    await fetch(`./api/user/findOne?username=${qs}`)
    .then(function(authorData) {
        return authorData.json();
    }).then(function(authorDataJSON) {
        loadedArtistID = authorDataJSON.UID;
        isLoggedIn ? findUserArr() : null;
    })
}

async function setArtPageInfo(artInfo) {
    loadedArtID = artInfo.AID;
    titleContainer.innerText = artInfo.title;
    authorContainer.innerText = artInfo.author;
    descContainer.innerText = artInfo.description;
    artDisplayContainer.src = artInfo.src;
}

async function findUserArr() {  // Find all relevant IDs for loaded art in current user's db
    await Promise.all([
        fetch(`./api/user/findIDInArrField`, {  // Likes field, searches with loaded AID
            method: "PATCH",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                username: sessionStorage.user,
                SID: loadedArtID,
                field: "likedPosts",
            })
        }),
        fetch(`./api/user/findIDInArrField`, {  // Favorites field, searches with loaded AID
            method: "PATCH",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                username: sessionStorage.user,
                SID: loadedArtID,
                field: "favoritePosts",
            })
        }),
        fetch(`./api/user/findIDInArrField`, {  // Followed field, will search with Author UID
            method: "PATCH",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                username: sessionStorage.user,
                SID: loadedArtistID,
                field: "followedUsers",
            })
        })
    ])
    .then((results) => {
        var likeRes = results[0].json();
        var favRes = results[1].json();
        var followRes = results[2].json();
        // If any evaluate to true, fill button with prefill set to true; persistant button statuses
        Promise.all([likeRes, favRes, followRes]).then((trimmed) => {
            trimmed[0].result ? toggleSocialButton("like", true) : null;
            trimmed[1].result ? toggleSocialButton("fav", true) : null;
            trimmed[2].result ? toggleSocialButton("follow", true) : null;
        })
    })
}
        

function drawCommentPrompt() {
    if (!isLoggedIn) return;
    commentPrompt.value = "";
    commentPrompt.classList.remove("hidden");
}

function toggleSocialButton(buttonName, prefill=false) {
    const icon = socialSection.querySelectorAll(`[name=${buttonName}]`)[0];
    if (icon.src == ASSET + buttonName + "-empty.png") {  // If button is empty, fill it
        icon.filled = true;
        icon.src = ASSET + buttonName + "-full.png"
    } 
    else { // If button is full, empty it
        icon.filled = false;
        icon.src = ASSET + buttonName + "-empty.png"
    }
    prefill ? null : socialButtonHandler(icon); // denotes whether button was filled automatically or by user action
}

async function socialButtonHandler(icon) {
    if (isLoggedIn) {
        let UIDGiven;
        iconActive = icon.filled;
        userDBField = userActionDict[icon.name];  // get name for field to change in users DB
        artDBField = artActionDict[icon.name];  // get name for field to change in art DB
        if (artDBField) { // IOW, does app change a field in ArtDB? Will fire on Like and Fav
            await modifyArtScoreFetch(iconActive, artDBField)
            .then((response) => {
                console.log(response);
                UIDGiven = false;
            })
        }
        else {  // Will fire on Follow
            await modifyFollowerCountFetch(iconActive)
            .then((response) => {
                console.log(response);
                UIDGiven = true;
            })
        }
        await modifyArrField(iconActive, userDBField, UIDGiven)  // call to modify current user's relevant field in DB
        .then((response) => {
            console.log(response);
        })
    }
    else { // User not logged in, cannot vote; bug them to make an account :)
        const userRes = confirm("You must be signed in to vote! Would you like to be redirected the signup form?");
        userRes ? window.location.href(`./signup.html`) : toggleSocialButton(icon.name, true);
    }
};

async function modifyArtScoreFetch(iconActive, artDBField) {
    await fetch(`./api/art/modifyArtScore`, {
        method: "PATCH",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
            willAdd: iconActive,
            AID: loadedArtID,
            field: artDBField,
        })
    }).then((response) => async function() {
        return response;
    })
}

async function modifyFollowerCountFetch(iconActive) {
    await fetch(`./api/user/modifyFollowerCount`, {
        method: "PATCH",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
            willAdd: iconActive,
            UID: loadedArtistID,
        })
    }).then((response) => async function() {
        return response;
    })
}

async function modifyArrField(iconActive, userField, UIDGiven=false) {
    let idValue = UIDGiven ? loadedArtistID : loadedArtID;
    if (iconActive) {
        await fetch(`./api/user/pushToArrField`, {
            method: "PATCH",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                username: sessionStorage.user,
                SID: idValue,
                field: userField,
            })
    })
    } 
    else {
        await fetch(`./api/user/pullFromArrField`, {
            method: "PATCH",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                username: sessionStorage.user,
                SID: idValue,
                field: userField,
            })
        })
    }
}

// Comment function

async function generateCommentElement(commentObj) {
    var commOutWindow = document.createElement("div");
    var usernameContainer = document.createElement("div");
    var commentOutString = document.createElement("p");
    var pfp = document.createElement("img");
    var nameText = document.createElement("span");
    var dateTime = document.createElement("span");
    commOutWindow.id = "commentContainer";
    commOutWindow.classList.add("hidden");
    usernameContainer.classList.add("user");
    pfp.id = "userImg";
    await fetch(`./api/user/findOneByID?UID=${commentObj.UID}`)
    .then((response) => response.json())
    .then((trimmed) => {
        const usernameNode = document.createTextNode(trimmed.username);
        pfp.src = trimmed.pfpSrc;
        nameText.id = "username";
        commentOutString.id = "comment";
        const prompt = document.createTextNode("> ");
        const node = document.createTextNode(commentObj.commentString);
        const tripleSpace = document.createTextNode("   ");
        const dateNode = document.createTextNode(commentObj.datePosted.slice(0, 10));
        const timeNode = document.createTextNode(commentObj.datePosted.slice(11)); 
        nameText.appendChild(usernameNode);
        dateTime.appendChild(dateNode);
        dateTime.appendChild(tripleSpace);
        dateTime.appendChild(timeNode);
        commentOutString.appendChild(prompt);
        commentOutString.appendChild(node);
        usernameContainer.appendChild(pfp);
        usernameContainer.appendChild(nameText);
        usernameContainer.appendChild(dateTime);
        commOutWindow.appendChild(usernameContainer);
        commOutWindow.appendChild(commentOutString);
        allComms.appendChild(commOutWindow);
    });
    
}

async function postComment() {
    let commentIn =  document.getElementById("commentBox").value;
    let loggedInUID;
    await fetch(`./api/user/findOne?username=${sessionStorage.user}`)
    .then((response) => response.json())
    .then((trimmed) => {
        loggedInUID = trimmed.UID;
    });
    await fetch("./api/comments/postComment", {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
            commentString: commentIn,
            AID: loadedArtID,
            UID: loggedInUID,
        })
    }).then((response) => {
        console.log(response);
        location.reload();
    })
}

async function loadComments() {
    await fetch(`./api/comments/getAllComments/?AID=${loadedArtID}`).then((response) => response.json())
    .then((trimmed) => {
            for (let i = 0; i < trimmed.length; i++) {
                generateCommentElement(trimmed[i]);
            }
            allComms.classList.remove("hidden");
    })
}

// Functions for slider

// var prev = document.getElementById("prev");
// var next = document.getElementById("next");
// var thumbar = document.getElementsByClassName("thumbar")
// var hero = document.getElementById("hero");

// var backgroundImg = new Array(
//     "pictures/yummycake.png",
//     "pictures/mountain.png",
//     "pictures/fall.png",
//     "pictures/colorsoflife.png",
//     "pictures/ice cream.jpg",
//     "pictures/happyhalloween.png",
//     "pictures/peaceful.png",
//     "pictures/dawn.png",
//     "pictures/blueworld.png",
//     "pictures/fish.jpg",
//     "The sunset over the sea",
//     "pictures/The bright moon shines over the sea.jpg",
//     "pictures/Sea Waves.jpg",
// );

// let i = 0;
// next.onclick = function(){
//     if (i < 4){
//         hero.style.backgroundImage = 'url("'+backgroundImg[i+1]+'")';
//         thumbar[i+1].classList.add(active);
//         thumbar[i].classList.remove("active");
//         i++;
//     }
// }

// prev.onclick = function(){
//     if(i>0){
//         hero.style.backgroundImge = 'url("'+backgroundImg[i-1]+'")';
//         thumbar[i-1].classList.add(active);
//         thumbar[i].classList.remove("active");
//         i--;
//     }
// }