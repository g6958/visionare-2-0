const mongoose = require("mongoose");

const commentSchema = new mongoose.Schema({
    commentString: String,
    datePosted: String,
    UID: String,
    AID: String,
});

const commentMeta = mongoose.model("commentMeta", commentSchema);
module.exports = commentMeta;