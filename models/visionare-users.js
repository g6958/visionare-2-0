const mongoose = require("mongoose");

const visionareUsersSchema = new mongoose.Schema({
    fName: String,
    lName: String,
    userAge: Number,
    username: String,
    password: String,
    contributor: Boolean,
    token: String,
    bio: String,
    pfpSrc: String,
    followers: Number,
    likedPosts: [String],
    favoritePosts: [String],
    followedUsers: [],
});

const visonareUser = mongoose.model("visionareUsers", visionareUsersSchema);
module.exports = visonareUser;