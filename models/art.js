const mongoose = require("mongoose");

const artMetaSchema = new mongoose.Schema({
    title: String,
    author: String,
    likes: Number,
    favorites: Number,
    description: String,
    src: String,
    tag1: String,
    tag2: String,
    tag3: String,
});

const artMeta = mongoose.model("artMeta", artMetaSchema);
module.exports = artMeta;